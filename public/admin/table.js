var firebaseConfig = {
    apiKey: "AIzaSyBdXFZiGB9uCmlI2VJSKRHwwyeYEowzC8M",
    authDomain: "proyectove-ffcdc.firebaseapp.com",
    databaseURL: "https://proyectove-ffcdc-default-rtdb.firebaseio.com",
    projectId: "proyectove-ffcdc",
    storageBucket: "proyectove-ffcdc.appspot.com",
    messagingSenderId: "657046213445",
    appId: "1:657046213445:web:877e44a1212772ebabe42c"
  };                                
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
function renderTable(){
    var order= firebase.database().ref("order/");
    order.on("child_added", function(data){
        var orderValue= data.val();
        document.getElementById("table").innerHTML+=`
            <tr>
                
                <td>${orderValue.order}</d>
                <td>${orderValue.total}</d>
                <td>${orderValue.date}</d>
                <td>${orderValue.hour}</d>
                <td>${orderValue.year}</d>
                <td>${orderValue.userNombre}</d>
                <td>${orderValue.userTelefono}</d>
                <td>${orderValue.userDireccion}</d>
                
                <td>${orderValue.products.map(function(product){
                    return `
                    <ul>
                        <li>${product.name}</li>
                        <li>${product.price} </li>
                    </ul>
                    `
                })}</d>
                <td>${orderValue.payment}</d>
            </tr>
        `;
    });
};

