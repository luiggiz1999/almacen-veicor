var app = new Vue({
    el: '#horario',
    data: {
        titulo: 'Nuestro horario de atencion',
        lunes: 'lunes 08:00–18:30',
        martes: 'martes 08:00–18:30',
        miercoles: 'miércoles 08:00–18:30',
        jueves: 'jueves 08:00–18:30',
        viernes: 'viernes 08:00–18:30',
        sabado: 'sábado 08:00–18:30',
        domingo: 'domingo-cerrado'
    }
})