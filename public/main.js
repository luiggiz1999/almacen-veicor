//window.addEventListener('load', function(){
    var firebaseConfig = {
        apiKey: "AIzaSyBdXFZiGB9uCmlI2VJSKRHwwyeYEowzC8M",
        authDomain: "proyectove-ffcdc.firebaseapp.com",
        databaseURL: "https://proyectove-ffcdc-default-rtdb.firebaseio.com",
        projectId: "proyectove-ffcdc",
        storageBucket: "proyectove-ffcdc.appspot.com",
        messagingSenderId: "657046213445",
        appId: "1:657046213445:web:877e44a1212772ebabe42c"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
     

      //GLOBAL
      var products=[];
      var cartItems=[];
      var cart_n= document.getElementById('cart_n');
      //DIVS
      var fruitDIV= document.getElementById("fruitDIV");
      var juiceDIV= document.getElementById("juiceDIV");
      var ConservaDIV= document.getElementById("ConservaDIV");
      var bebidaCalienteDIV= document.getElementById("bebidaCalienteDIV");
      var lacteoDIV= document.getElementById("lacteoDIV");


      //INFORMACIÓN                            

      var COSINA=[
          {name:'Sanducheras', price:15},
          {name:'3 Ollas enlozadas', price:13},                     
          {name:'3 Ollas imguso', price:20},
          {name:'Cafetera', price:30},
          {name:'Termos', price:24},    
    
    
    ];                               
      var HOGAR=[
          {name:'Diseño de 2 elefantes', price:17},
          {name:'Diseño de un elefante', price:14},
          {name:'Almohadas nordicas', price:10},
          {name:'Reloj de pared', price:7}, 
          {name:'Decoraciones de porcelana', price:5},        
    
    
    ];
    var JARDINERIA=[
        {name:'Macetero grande', price:12},
        {name:'Macetero mediano', price:7},
        {name:'Macetero pequeño', price:4},
        {name:'Maceta Redonda', price:5},
        {name:'Macetero Marca PIKA', price:6},
    ];
    var BANIO=[
        {name:'Alcohol Antibacterial', price:10},
        {name:'Desinfectante 4 litros', price:5},
        {name:'Jabon Liquido', price:5},
        {name:'Cloro', price:2},
        {name:'Desinfectante Estrella', price:2},
    ];
    var SALA=[
        {name:'Platos desechables', price:2},
        {name:'Globos', price:3},
        {name:'Funda de regalos', price:1},
        {name:'Vasos desechables', price:1},
        {name:'Vaso Domo', price:2},
        
        
    ]

    //HTML
    function HTMLfruitProduct(con){
        let URL=`img/hogar/mo${con}.jpg`;
        let btn = `btnFruit${con}`;
        return `
            <div class="col-md-4">
                <div class= "card mb-4 shadow-sm">
                    <img class="card-img-top" style="height:16rem;" src="${URL}"
                    alt="Card image cap">
                    <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>                     
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <p class="card-text">${HOGAR[con-1].name}</p>
                        <p class="card-text">Price: ${HOGAR[con-1].price}.00</p>        
                        <div class="d-flex justify-content-between
                        align-items-center">
                            <div class="btn-group"> 
                                <button type="button" onclick="cart2('${HOGAR[con-1]
                                .name}', '${HOGAR[con-1].price}','${URL}', '${con}', 
                                '${btn}')"
                                class="btn btn-sm btn-outline-secondary"> 
                                <a href="cart.html" style="color:inherit;"> Comprar Ahora </a> </button>

                                <button id="${btn}" type="button" onclick="cart('${HOGAR[con-1]
                                .name}', '${HOGAR[con-1].price}', '${URL}', '${con}', '${btn}')"
                                class="btn btn-sm 
                                btn-outline-secondary"> Agregar </button>
                            </div>
                            <small class="text-muted"> Envío con Recargo </small>
                        </div>
                    </div>
                </div>                                           
            </div>
        `
    }

    function HTMLbebidaProduct(con){
        let URL=`img/cosina/fur${con}.jpg`;
        let btn = `btnBebida${con}`;
        return `
            <div class="col-md-4">
                <div class= "card mb-4 shadow-sm">
                    <img class="card-img-top" style="height:16rem;" src="${URL}"
                    alt="Card image cap">
                    <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>                     
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <p class="card-text">${COSINA[con-1].name}</p>
                        <p class="card-text">Price: ${COSINA[con-1].price}.00</p>
                        <div class="d-flex justify-content-between
                        align-items-center">
                            <div class="btn-group"> 
                                <button type="button" onclick="cart2('${COSINA[con-1]
                                .name}', '${COSINA[con-1].price}','${URL}', '${con}', 
                                '${btn}')"
                                class="btn btn-sm btn-outline-secondary"> 
                                <a href="cart.html" style="color:inherit;"> Comprar Ahora </a></button>

                                <button id="${btn}" type="button" onclick="cart('${COSINA[con-1]
                                .name}', '${COSINA[con-1].price}', '${URL}', '${con}', '${btn}')"
                                class="btn btn-sm 
                                btn-outline-secondary"> Agregar  </button>
                            </div>
                            <small class="text-muted"> Envío con Recargo </small>
                        </div>
                    </div>
                </div>                                           
            </div>
        `
    }
    function HTMLconservaProduct(con){
        let URL=`img/jardineria/ve${con}.jpg`;
        let btn = `btnConserva${con}`;
        return `
            <div class="col-md-4">
                <div class= "card mb-4 shadow-sm">
                    <img class="card-img-top" style="height:16rem;" src="${URL}"
                    alt="Card image cap">
                    <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>                     
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <p class="card-text">${JARDINERIA[con-1].name}</p>
                        <p class="card-text">Price: ${JARDINERIA[con-1].price}.00</p>
                        <div class="d-flex justify-content-between
                        align-items-center">
                            <div class="btn-group"> 
                                <button type="button" onclick="cart2('${JARDINERIA[con-1]
                                .name}', '${JARDINERIA[con-1].price}','${URL}', '${con}', 
                                '${btn}')"
                                class="btn btn-sm btn-outline-secondary"> 
                                <a href="cart.html" style="color:inherit;"> Comprar Ahora </a></button>

                                <button id="${btn}" type="button" onclick="cart('${JARDINERIA[con-1]
                                .name}', '${JARDINERIA[con-1].price}', '${URL}', '${con}', '${btn}')"
                                class="btn btn-sm 
                                btn-outline-secondary"> Agregar  </button>
                            </div>
                            <small class="text-muted"> Envío con Recargo </small>
                        </div>
                    </div>
                </div>                                           
            </div>
        `
    }

    function HTMLbebiCalProduct(con){
        let URL=`img/baño/ca${con}.png`;
        let btn = `btnBebiCal${con}`;
        return `
            <div class="col-md-4">
                <div class= "card mb-4 shadow-sm">
                    <img class="card-img-top" style="height:16rem;" src="${URL}"
                    alt="Card image cap">
                    <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>                     
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <p class="card-text">${BANIO[con-1].name}</p>
                        <p class="card-text">Price: ${BANIO[con-1].price}.00</p>        
                        <div class="d-flex justify-content-between
                        align-items-center">
                            <div class="btn-group"> 
                                <button type="button" onclick="cart2('${BANIO[con-1]
                                .name}', '${BANIO[con-1].price}','${URL}', '${con}', 
                                '${btn}')"
                                class="btn btn-sm btn-outline-secondary"> 
                                <a href="cart.html" style="color:inherit;"> Comprar Ahora </a> </button>

                                <button id="${btn}" type="button" onclick="cart('${BANIO[con-1]
                                .name}', '${BANIO[con-1].price}', '${URL}', '${con}', '${btn}')"
                                class="btn btn-sm 
                                btn-outline-secondary"> Agregar </button>
                            </div>
                            <small class="text-muted"> Envío con Recargo </small>
                        </div>
                    </div>
                </div>                                           
            </div>
        `
    }

    function HTMLlacteoProduct(con){
        let URL=`img/sala/cam${con}.jpg`;
        let btn = `btnLacteo${con}`;
        return `
            <div class="col-md-4">
                <div class= "card mb-4 shadow-sm">
                    <img class="card-img-top" style="height:16rem;" src="${URL}"
                    alt="Card image cap">
                    <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <i style="color:orange;" class="fa fa-star"> </i>                     
                        <i style="color:orange;" class="fa fa-star"> </i>
                        <p class="card-text">${SALA[con-1].name}</p>
                        <p class="card-text">Price: ${SALA[con-1].price}.00</p>        
                        <div class="d-flex justify-content-between
                        align-items-center">
                            <div class="btn-group"> 
                                <button type="button" onclick="cart2('${SALA[con-1]
                                .name}', '${SALA[con-1].price}','${URL}', '${con}', 
                                '${btn}')"
                                class="btn btn-sm btn-outline-secondary"> 
                                <a href="cart.html" style="color:inherit;"> Comprar Ahora </a> </button>

                                <button id="${btn}" type="button" onclick="cart('${SALA[con-1]
                                .name}', '${SALA[con-1].price}', '${URL}', '${con}', '${btn}')"
                                class="btn btn-sm 
                                btn-outline-secondary"> Agregar </button>
                            </div>
                            <small class="text-muted"> Envío con Recargo </small>
                        </div>
                    </div>
                </div>                                           
            </div>
        `
    }


     //ANIMATION
     function animation(){
        const toast = swal.mixin({
            toast:true,
            position:'top-end',
            showConfirmButton: false,
            timer:2000
        });
        toast({
            type:'success',
            title:'Added to shopping cart'
        })
    }

    //CART FUNCTIONS  
    function cart(name,price,url,con,btncart){
        var item={
            name:name,
            price:price,
            url:url
        }
        cartItems.push(item);
        let storage = JSON.parse(localStorage.getItem("cart"));
        if(storage == null){
            products.push(item);
            localStorage.setItem("cart", JSON.stringify(products));

        }else{
            products = JSON.parse(localStorage.getItem("cart"));
            products.push(item);
            localStorage.setItem("cart",JSON.stringify(products));
           
        }
        products = JSON.parse(localStorage.getItem('cart'));
        cart_n.innerHTML = `[${products.length}]`;
        document.getElementById(btncart).style.display='none';
        animation(); 
    }
    //  CAMBIO END

   function cart2(name,price,url,con,btncart){
    var item={
        name:name,
        price:price,
        url:url
    }
    cartItems.push(item);
    let storage = JSON.parse(localStorage.getItem("cart"));
    if(storage == null){
        products.push(item);
        localStorage.setItem("cart", JSON.stringify(products));

    }else{
        products = JSON.parse(localStorage.getItem("cart"));
        products.push(item);
        localStorage.setItem("cart",JSON.stringify(products));
       
    }
    products = JSON.parse(localStorage.getItem('cart'));
    cart_n.innerHTML = `[${products.length}]`;
    document.getElementById(btncart).style.display='none';
    
   }
            
    //RENDER
    function render(){
        for(let index =1; index <=5; index++){
            lacteoDIV.innerHTML+=`${HTMLlacteoProduct(index)}`;

        }
        for(let index =1; index <=5; index++){
            juiceDIV.innerHTML+=`${HTMLbebidaProduct(index)}`;
            ConservaDIV.innerHTML+=`${HTMLconservaProduct(index)}`;
            bebidaCalienteDIV.innerHTML+=`${HTMLbebiCalProduct(index)}`;
            fruitDIV.innerHTML+=`${HTMLfruitProduct(index)}`;
        }
        
        if(localStorage.getItem("cart")==null){

        }else{
            products=JSON.parse(localStorage.getItem("cart"));
            cart_n.innerHTML=`[${products.length}]`;
        }
    }


   
//})